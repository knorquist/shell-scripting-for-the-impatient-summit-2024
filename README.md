# Shell Scripting For The Impatient Summit 2024

## Getting Started
If you're participating in the [summit workshop](https://gitlab.com/gitlab-com/marketing/corporate_marketing/contribute/las-vegas-2024/-/issues/231), welcome! Please clone this repo by running `git clone git@gitlab.com:knorquist/shell-scripting-for-the-impatient-summit-2024.git` to get started. Please also consider attending [Safe Shell Scripting](https://gitlab.com/gitlab-com/marketing/corporate_marketing/contribute/las-vegas-2024/-/issues/216) for even more shell fun!

## Description
This repo includes example files and instructions to follow while participating in our workshop. Each prompt is intended to prepare you for the next, but you are free to complete them in whichever order you would like.

## Support
Please consider joining the Slack channel [#summit-fy25-shell-scripting-for-the-impatient](https://gitlab.enterprise.slack.com/archives/C06JX1KV7J7) to chat with other participants and share your work!

## License
This project is open source under the MIT License. See [LICENSE](https://gitlab.com/knorquist/shell-scripting-for-the-impatient-summit-2024/-/blob/main/LICENSE) for more info.

---

## Setting Up Zsh and Oh My Zsh
These instructions assume you don't already have `zsh` installed, and are either running on a Debian-based distro (such as Ubuntu) or macOS. Choose the most appropriate instructions for your environment. If in doubt, refer to the official docs at the bottom of this section, or hit up our [Slack channel]((https://gitlab.enterprise.slack.com/archives/C06JX1KV7J7)).

### Why Oh My Zsh?
[Oh My Zsh](https://ohmyz.sh/) is a framework to manage `zsh`. It makes configuring `zsh` much easier and lets you do cool things like install themes and plugins (of which it includes *several* by default). You want Oh My Zsh.

### Prerequisites
* `curl` or `wget`
* `git` v2.4.11+
* For macOS, [Homebrew](https://brew.sh/)
* A terminal emulator. For Linux, the default should be adequate. For macOS consider [iTerm2](https://iterm2.com/index.html)


### Installation
Note: You should always check scripts before running them on your machine. These instructions are provided for convenience, but you are responsible for your machine. Script responsibly.

1. Optional but highly recommend: Install patched fonts (these include additional glyphs)
    * Debian-based: `apt update && apt install fonts-powerline`
    * macOS:
    ```
    # clone
    git clone https://github.com/powerline/fonts.git --depth=1
    # install
    cd fonts
    ./install.sh
    # clean-up a bit
    cd ..
    rm -rf fonts
    ```
    * Or manually install the provided fonts in `~/fonts`
    * In your terminal emulator, find Settings/Preferences and update your font to one of these patched fonts. For the default Ubuntu terminal, see the below screenshots:
    ![Screenshot of default Ubuntu terminal with hamburger menu selected](image-01.png)
    ![Screenshot of Preferences menu for Ubuntu terminal with a custom profile configured with a custom font set](image-02.png)
1. Install `zsh`
    * Debian-based: `apt update && apt install zsh -y`
    * macOS: `brew install zsh`
1. Make `zsh` your default shell: `chsh -s $(which zsh)`
1. Restart your terminal session to make sure your default shell was set to `zsh`.
1. Install Oh My Zsh: `sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`
1. Optional: Install my favorite theme, [powerlevel10k](https://github.com/romkatv/powerlevel10k):
    * `git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k`
    * Edit `~/.zshrc` and set `ZSH_THEME="powerlevel10k/powerlevel10k"`
    * Restart your terminal session and the config wizard should automatically start, otherwise run `p10k configure`


### Official Docs Links
* [Install Homebrew](https://brew.sh/)
* [Zsh Official Site](https://www.zsh.org/)
* [Zsh User's Guide](https://zsh.sourceforge.io/Guide/zshguide.html)
* [Oh My Zsh wiki](https://github.com/ohmyzsh/ohmyzsh/wiki)
* [Powerline fonts](https://github.com/powerline/fonts)
* [Powerlevel10k repo](https://github.com/romkatv/powerlevel10k)