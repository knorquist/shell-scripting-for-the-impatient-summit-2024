#!/bin/bash
#
# This script initializes a directory with some
# directories and text files of various
# extensions for practicing our bash-fu.

make_practice_dir() {
    mkdir practice_dir && cd $_
}

make_text_files() {
    touch "file-"{1..6}.txt
    for file in "*.txt"; do
        echo "I'm a text file!" | tee $file
    done
}

make_py_files() {
    touch "file-"{1..3}.py
    for file in "*.py"; do
        echo -e "def main()\n    pass\n\nif __name__ == '__main__':\n    main()\n" | tee $file
    done
    mkdir "py"
}

make_bash_files() {
    touch "file-"{1..9}.bash
    for file in "*.bash"; do
        echo "I don't have my shebang, uh oh!" | tee $file
    done
}

make_gotcha_files() {
    echo "I don't have an extension." > "no_extension_file"
    echo "My filename has spaces, that's bad." > "file with spaces oh no.txt"
}

make_archive_file() {
    echo "Hello World!" > \
    "to_be_zipped_file.txt" && tar -czvf "archive.tar.gz" "to_be_zipped_file.txt"
}

# "main"
make_practice_dir
make_text_files
make_py_files
make_bash_files
make_gotcha_files
make_archive_file

exit